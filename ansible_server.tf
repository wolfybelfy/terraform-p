provider "aws" {
alias  = "ansible-git"
region = "ap-south-1" 
 }

resource "aws_instance" "ansible_server" {
  ami           = "ami-03bb6d83c60fc5f7c" 
  instance_type = "t2.micro"
  key_name      = "terraform-kp" 

  tags = {
    Name = "AnsibleServer"
  }
}

resource "null_resource" "ansible_installation" {
  triggers = {
    ansible_server_id = aws_instance.ansible_server.id
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("C:/Users/up310/Downloads/terraform-kp.pem")
      host        = aws_instance.ansible_server.public_ip
    }

    inline = [
      "sudo yum install epel-release -y",
      "sudo yum install ansible -y"
    ]
  }
}
