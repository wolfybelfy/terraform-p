provider "aws" {
  region = "ap-south-1"
}

resource "aws_dynamodb_table" "traffic_monitoring" {
  name           = "TrafficMonitoringTable"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "ID"

  attribute {
    name = "ID"
    type = "S"
  }

  tags = {
    Purpose = "Infrastructure Monitoring"
  }
}
