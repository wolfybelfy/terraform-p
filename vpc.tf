# provider "aws" {
#   alias  = "alternate-up0"
#   region = "ap-south-1" 
#   shared_config_files      = ["/root/.aws/config"]
#   shared_credentials_files = ["/root/.aws/credentials"]
# }

# resource "aws_vpc" "custom_default_vpc" {
#   cidr_block           = "10.0.0.0/16"
#   enable_dns_support   = true
#   enable_dns_hostnames = true

#   tags = {
#     Name = "Custom Default VPC"
#   }
# }

# resource "aws_internet_gateway" "gw" {
#   vpc_id = aws_vpc.custom_default_vpc.id

#   tags = {
#     Name = "Custom Default IGW"
#   }
# }

# resource "aws_subnet" "custom_default_subnet" {
#   count                   = length(data.aws_availability_zones.available.names)
#   vpc_id                  = aws_vpc.custom_default_vpc.id
#   cidr_block              = cidrsubnet(aws_vpc.custom_default_vpc.cidr_block, 8, count.index)
#   map_public_ip_on_launch = true 
#   availability_zone       = element(data.aws_availability_zones.available.names, count.index)

#   tags = {
#     Name = "Custom Default Subnet ${count.index}"
#   }
# }

# data "aws_availability_zones" "available" {
#   state = "available"
# }

# resource "aws_route_table" "custom_default_rt" {
#   vpc_id = aws_vpc.custom_default_vpc.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.gw.id
#   }

#   tags = {
#     Name = "Custom Default Route Table"
#   }
# }

# resource "aws_route_table_association" "a" {
#   count          = length(aws_subnet.custom_default_subnet.*.id)
#   subnet_id      = aws_subnet.custom_default_subnet.*.id[count.index]
#   route_table_id = aws_route_table.custom_default_rt.id
# }
