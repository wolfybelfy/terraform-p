# terraform {
#   required_providers {
#     aws = {
#       source = "hashicorp/aws"
#       version = "5.41.0"
#     }
#   }
# }

# provider "aws"  {
#  region = var.region
#  shared_config_files      = ["/root/.aws/config"]
#  shared_credentials_files = ["/root/.aws/credentials"]
#  }


# resource "aws_instance" "my_instance" {
#     ami = var.ami
#     instance_type = var.instance_type
#     # count = var.count
#     key_name = var.key_name
#     tags = var.tags
# }

# variable "region" {
#     type = string
#     description = "this is region value"
#     default =  "ap-south-1"
# }

# variable "ami" {
#     type = string
#     description = "this is ami image"
#     default= "ami-03bb6d83c60fc5f7c"
# }


# variable "instance_type" {
#     type = string
#     description = "this is instance type"
#     default="t2.micro"
  
# }

# # variable "count" {
# #   type = string
# #   description = "getting instance count"
# #   default = "2"
# # }

# variable "key_name" {
#     type = string
#     description = "this is my key pair"
#     default="terraform-kp"
# }

# variable "tags" {
#   type = map
#   description = "tags vaalues"
#   default = {
#         Name = "ubuntu_variable"
#         environment = "production"
#   }
#  }