# provider "aws" {
# alias  = "alternate-up012"
#   region = "ap-south-1"
# }

# resource "aws_vpc" "example_vpc" {
#   cidr_block       = "10.0.0.0/16"
#   enable_dns_support   = true
#   enable_dns_hostnames = true

#   tags = {
#     Name = "example-vpc"
#   }
# }

# resource "aws_subnet" "example_subnet" {
#   vpc_id                  = aws_vpc.example_vpc.id
#   cidr_block              = "10.0.1.0/24"
#   map_public_ip_on_launch = true
#   availability_zone       = "ap-south-1a"

#   tags = {
#     Name = "example-subnet"
#   }
# }

# resource "aws_internet_gateway" "example_igw" {
#   vpc_id = aws_vpc.example_vpc.id

#   tags = {
#     Name = "example-igw"
#   }
# }

# resource "aws_route_table" "example_rt" {
#   vpc_id = aws_vpc.example_vpc.id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.example_igw.id
#   }

#   tags = {
#     Name = "example-rt"
#   }
# }

# resource "aws_route_table_association" "example_rta" {
#   subnet_id      = aws_subnet.example_subnet.id
#   route_table_id = aws_route_table.example_rt.id
# }

# resource "aws_security_group" "example_sg" {
#   name        = "example-sg"
#   description = "Allow SSH inbound traffic"
#   vpc_id      = aws_vpc.example_vpc.id

#   ingress {
#     description = "SSH"
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "example-sg"
#   }
# }

# resource "aws_instance" "example_instance" {
#   ami           = "ami-007020fd9c84e18c7"
#   instance_type = "t2.micro"
#   key_name      = "terraform-kp"
#   subnet_id     = aws_subnet.example_subnet.id
#   security_groups = [aws_security_group.example_sg.id]

#   tags = {
#     Name = "example-instance"
#   }
# }

# output "instance_public_ip" {
#   value = aws_instance.example_instance.public_ip
# }
