provider "aws" {
  alias  = "alternate-upw"
  region = "ap-south-1" 
}

resource "aws_instance" "git_server" {
  ami           = "ami-03bb6d83c60fc5f7c"
  instance_type = "t2.micro"
  key_name      = "terraform-kp" 
  tags = {
    Name = "GitServer"
  }
}
